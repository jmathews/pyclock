import settings
import pygame
from time import sleep
from datetime import datetime
from os import path, listdir, makedirs
from random import choice


class pyclock:
    screen = None
    current_image_filename = None
    image_folder = None
    current_image = None
    settings = {
        "FULLSCREEN" : True,
        "IMAGES_FOLDER" : "images",
        "SECONDS_PER_IMAGE" : 60,
        "LIMIT_ITERATIONS_FOR_DEBUGGING" : False,
        "DEBUGGING_ITERATION_LIMIT" : 5,
        "USE_OVERRIDE_IMAGES_SUBFOLDER_FOR_DEBUGGING" : False,
        "OVERRIDE_IMAGES_SUBFOLDER_FOR_DEBUGGING" : "images/00/00",
        }

    def run_clock(self):
        if self.settings["LIMIT_ITERATIONS_FOR_DEBUGGING"]:
            for iterations in range(1, self.settings["DEBUGGING_ITERATION_LIMIT"] + 1):
                self.update_clock()
        else:
            while True:
                self.update_clock()

    def __init__(self):
        self.load_settings()
        self.setup_screen()
        original_mouse_state = pygame.mouse.set_visible(False)
        self.run_clock()
        pygame.mouse.set_visible(original_mouse_state)

    def load_setting(self, setting):
        if setting in self.settings:
            self.settings[setting] = settings.__dict__[setting]

    def load_settings(self):
        for setting in settings.__dict__:
            self.load_setting(setting)

    @staticmethod
    def get_screen_size():
        video_info = pygame.display.Info()
        height = video_info.current_h
        width = video_info.current_w
        return width, height

    def get_screen_flags(self):
        flags = 0;
        if self.settings["FULLSCREEN"]:
            flags = flags | pygame.FULLSCREEN
        return flags

    def setup_screen(self):
        pygame.init()
        size = self.get_screen_size()
        flags = self.get_screen_flags()
        self.screen = pygame.display.set_mode(size, flags)

    def update_clock(self):
        self.display_image_for_current_time()
        self.wait_for_next_image_time()

    def wait_for_next_image_time(self):
        sleep(self.settings["SECONDS_PER_IMAGE"])

    def display_image_for_current_time(self):
        self.get_next_image_filename()
        self.display_image_from_file()

    def put_image_on_screen(self):
        if self.current_image and self.screen:
            centered_origin = self.get_centered_origin()
            self.screen.blit(self.current_image, centered_origin)
            pygame.display.flip()

    def display_image_from_file(self):
        if self.current_image_filename:
            self.current_image = pygame.image.load(self.current_image_filename)
            self.put_image_on_screen()

    def get_centered_origin(self):
        if self.current_image and self.screen:
            (image_width, image_height) = self.current_image.get_size()
            (screen_width, screen_height) = self.screen.get_size()
            centered_origin_top = self.get_centered_dimension(image_height, screen_height)
            centered_origin_left = self.get_centered_dimension(image_width, screen_width)
            return centered_origin_left, centered_origin_top
        else:
            return None

    @staticmethod
    def get_centered_dimension(inner_item, outer_item):
        return (outer_item / 2) - (inner_item / 2)

    def get_next_image_filename(self):
        self.get_next_image_folder_name()
        self.get_random_image_file_from_folder()

    def set_current_image_filename(self, filename):
        folder_and_filename = path.join(self.image_folder, filename)
        if folder_and_filename and path.isfile(folder_and_filename):
            self.current_image_filename = folder_and_filename

    def get_random_image_from_file_list(self, files):
        filename = choice(files)
        if filename:
            self.set_current_image_filename(filename)

    def get_random_image_file_from_folder(self):
        if self.image_folder:
            files = listdir(self.image_folder)
            if files:
                self.get_random_image_from_file_list(files)

    def get_next_image_folder_name_candidate(self):
        if self.settings["USE_OVERRIDE_IMAGES_SUBFOLDER_FOR_DEBUGGING"]:
            image_folder = self.settings["OVERRIDE_IMAGES_SUBFOLDER_FOR_DEBUGGING"]
        else:
            current_hour, current_minute = self.get_hour_and_minute()
            image_folder = path.join(self.settings["IMAGES_FOLDER"], current_hour, current_minute)
        return image_folder

    def create_and_set_image_folder(self, image_folder):
        makedirs(image_folder)
        if image_folder and path.isdir(image_folder):
            self.image_folder = image_folder
        else:
            raise Exception("Expected a folder name, got {image_folder}".format(image_folder=image_folder))

    def set_image_folder(self, image_folder):
        if image_folder and path.isdir(image_folder):
            self.image_folder = image_folder
        else:
            self.create_and_set_image_folder(image_folder)

    def get_next_image_folder_name(self):
        image_folder = self.get_next_image_folder_name_candidate()
        self.set_image_folder(image_folder)

    def get_hour_and_minute(self):
        current_time = datetime.now()
        current_hour = self.get_2_digit_string(current_time.hour)
        current_minute = self.get_2_digit_string(current_time.minute)
        return current_hour, current_minute

    def get_2_digit_string(self, number):
        return str(number).zfill(2)

clock_instance = pyclock()