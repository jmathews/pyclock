from PIL import Image, ImageDraw, ImageFont
from os import path


def get_centered_dimension(inner_item, outer_item):
    return (outer_item / 2) - (inner_item / 2)

screen_dimensions = (1280, 800)

font = ImageFont.truetype("/Library/Fonts/Courier New.ttf", 250)

for hour in range(0, 24):
    for minute in range(0, 60):
        time_string = str(hour) + ":" + str(minute).zfill(2)
        time_string_dimensions = font.getsize(time_string)
        left = get_centered_dimension(time_string_dimensions[0], screen_dimensions[0])
        top = get_centered_dimension(time_string_dimensions[1], screen_dimensions[1])
        objIm = Image.new("RGB", screen_dimensions, None)
        draw = ImageDraw.Draw(objIm)
        draw.text((left, top), time_string, font=font)
        filename = path.join("images", str(hour).zfill(2), str(minute).zfill(2), "_generated_sample_image.jpg")
        objIm.save(filename, "JPEG")
