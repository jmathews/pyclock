FULLSCREEN = False
IMAGES_FOLDER = "images" #This can be a full path or a path relative to pyclock.py
SECONDS_PER_IMAGE = 1 #Set this to 1 for a new image every second, 60 for a new image every minute, or somewhere in between
LIMIT_ITERATIONS_FOR_DEBUGGING = True
DEBUGGING_ITERATION_LIMIT = 5
USE_OVERRIDE_IMAGES_SUBFOLDER_FOR_DEBUGGING = False
OVERRIDE_IMAGES_SUBFOLDER_FOR_DEBUGGING = "images/00/00"
